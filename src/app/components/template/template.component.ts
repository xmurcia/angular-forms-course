import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styles: [`
    .ng-invalid.ng-touched:not(form) {
      border: 1px solid red;
    }
  `]
})
export class TemplateComponent {

  countries: Array<string> = ['españa', 'portugal', 'francia', 'marruecos'];

  sex: String[] = ['Hombre', 'Mujer'];

  usuario: Object = {
    nombre: '',
    apellido: '',
    correo: '',
    pais : '',
    sexo : '',
    state : '',
  };

  constructor() { }

  guardar (data: NgForm) {
    console.log('Data', data);
    console.log('Usuario', this.usuario);
  }

}
