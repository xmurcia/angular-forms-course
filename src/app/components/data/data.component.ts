import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormArrayName } from '@angular/forms';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styles: []
})
export class DataComponent {

  forma: FormGroup;

  existingName: Array<string> = ['strider', 'xavili', 'pepe', 'maria'];

  usuario: Object = {
    nombreCompleto : {
      nombre : 'Fernando',
      apellido : 'Herrera',
    },
    correo : 'fherrera@gmail.com',
   pasatiempos : ['Correr'],
   password : '',
   password2 : '',
  };

  constructor() {
    this.forma = new FormGroup({
      'nombreCompleto' : new FormGroup({
        'nombre' : new FormControl('', [Validators.required, Validators.minLength(3)]),
        'apellido' : new FormControl('', [Validators.required, this.noHerrera]),
      }),
      'correo' : new FormControl('', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      'pasatiempos' : new FormArray([
        new FormControl('Correr', [ Validators.required]),
      ]),
      'password' : new FormControl('', Validators.required),
      'password2' : new FormControl(),
      'username' : new FormControl('', Validators.required, this.existeUsuario.bind( this.existingName )),
    });


    // Whit that observable, we are aware about all changes in the input boxes.
    this.forma.valueChanges.subscribe(
      data => {
        // console.log(data);
      }
    );

    // Now we are aware about any change in an specific input box
    this.forma.controls.username.valueChanges.subscribe(
      data => {
        console.log(data);
      }
    );

    this.forma.controls.password2.setValidators([Validators.required, this.passwordValidator.bind( this.forma )]);
    // Set default value for boxes
    // this.forma.setValue( this.usuario );
  }

  saveItems () {
    console.log( this.forma.value );
    console.log( this.forma );
    // this.forma.reset( this.usuario );
  }

  agregarPasatiempo () {
    (<FormArray>this.forma.controls.pasatiempos).push( new FormControl('', Validators.required));
  }

  noHerrera ( control: FormControl): { [s: string]: boolean } {
    if ( control.value === 'herrera') {
      return {
        noherrera : true
      };
    }
    // Si devuelve null, quiere decir que la validacion pasa si devuelve algo es que no pasa
    return null;
  }

  passwordValidator ( control: FormControl): { [s: string]: boolean } {

    const forma: any = this;

    if ( control.value !== forma.controls.password.value) {
      return {
        noiguales : true
      };
    }
    // Si devuelve null, quiere decir que la validacion pasa si devuelve algo es que no pasa
    return null;
  }

  existeUsuario ( control: FormControl): Promise<any> | Observable<any> {
    const existe: any = this;
    const promise = new Promise(
      ( resolve, reject ) => {
        setTimeout( () => {
          if ( existe.includes(control.value) ) {
            return resolve( { existe : true} );
          } else {
            resolve( null );
          }
        }, 2000);
    });

    return promise;
  }
}
